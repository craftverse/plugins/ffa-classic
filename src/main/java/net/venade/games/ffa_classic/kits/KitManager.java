package net.venade.games.ffa_classic.kits;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import net.venade.games.ffa_classic.FFAClassicPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @author JakeMT04
 * @since 15/07/2021
 */
public class KitManager {

  private final FFAClassicPlugin plugin;
  private final HashMap<String, Kit> kits = new HashMap<>();

  public KitManager(FFAClassicPlugin plugin) {
    this.plugin = plugin;
    this.load();
  }

  public List<Kit> getKits() {
    return new ArrayList<>(kits.values());
  }

  private void load() {
    ConfigurationSection section = this.plugin.getConfig().getConfigurationSection("kits");
    if (section == null) {
      return;
    }
    for (String name : section.getKeys(false)) {
      Kit kit = this.loadKit(name);
      this.kits.put(name.toLowerCase(), kit);
      if (kit.getArmorStandLocation() != null) {
        this.generateArmorStand(kit);
      }
      this.plugin.getLogger().info("Loaded kit " + name);
    }
  }

  public boolean kitExists(String name) {
    return kits.containsKey(name.toLowerCase());
  }

  public Kit getKit(String name) {
    if (kits.containsKey(name.toLowerCase())) {
      return kits.get(name.toLowerCase());
    }
    return null;

  }

  private Kit loadKit(String name) {
    ConfigurationSection config = this.plugin.getConfig()
        .getConfigurationSection("kits." + name.toLowerCase());
    if (config == null) {
      throw new IllegalArgumentException("Kit does not exist");
    }
    Kit kit = new Kit();
    String properName = config.getString("name", name);
    ItemStack helmet = config.getItemStack("helmet");
    ItemStack chestplate = config.getItemStack("chestplate");
    ItemStack leggings = config.getItemStack("leggings");
    ItemStack boots = config.getItemStack("boots");
    Location armorStandSpawn = config.getLocation("armor-stand");
    ConfigurationSection configItems = config.getConfigurationSection("items");
    ConfigurationSection configEffects = config.getConfigurationSection("effects");
    TreeMap<Integer, ItemStack> items = new TreeMap<>();
    for (String key : configItems.getKeys(false)) {
      ItemStack itemStack = configItems.getItemStack(key);
      itemStack = unbreakable(itemStack);
      items.put(Integer.parseInt(key), itemStack);
    }
    HashMap<PotionEffectType, Integer> potionEffects = new HashMap<>();
    if (configEffects != null) {
      for (String key : configEffects.getKeys(false)) {
        int level = configEffects.getInt(key);
        potionEffects.put(PotionEffectType.getByName(key), level);

      }
    }
    helmet = unbreakable(helmet);
    chestplate = unbreakable(chestplate);
    leggings = unbreakable(leggings);
    boots = unbreakable(boots);

    kit.setArmorStandLocation(armorStandSpawn);
    kit.setName(properName);
    kit.setHelmet(helmet);
    kit.setChestplate(chestplate);
    kit.setLeggings(leggings);
    kit.setBoots(boots);
    kit.setInventory(items);
    kit.setPotionEffect(potionEffects);

    return kit;
  }

  public ItemStack unbreakable(ItemStack stack) {
    if(stack == null) return null;
      ItemMeta meta = stack.getItemMeta();
      meta.setUnbreakable(true);
      stack.setItemMeta(meta);
    return stack;
  }

  public void generateArmorStand(Kit kit) {
    Location location = kit.getArmorStandLocation();
    if (location == null) {
      return;
    }
    this.plugin.getMapManager().getActiveWorld().getEntitiesByClass(ArmorStand.class)
        .forEach(e -> {
          if (e.hasMetadata("ffa-kitselect")) {
            String kitName = e.getMetadata("ffa-kitselect").get(0).asString();
            if (kit.getName().equalsIgnoreCase(kitName)) {
              e.remove();
            }
          }
        });
    this.plugin.getMapManager().getActiveWorld().spawn(location, ArmorStand.class, stand -> {
      stand.setMetadata("ffa-kitselect", new FixedMetadataValue(this.plugin, kit.getName()));
      stand.setArms(true);
      stand.setGravity(false);
      stand.setBasePlate(false);
      stand.setCustomName(ChatColor.AQUA.toString() + ChatColor.BOLD + kit.getName());
      stand.setCustomNameVisible(true);
      stand.setPersistent(false);
      if (stand.getEquipment() == null) {
        return;
      }
      if (kit.getHelmet() != null) {
        stand.getEquipment().setHelmet(kit.getHelmet().clone(), true);
      }
      if (kit.getChestplate() != null) {
        stand.getEquipment().setChestplate(kit.getChestplate().clone(), true);
      }
      if (kit.getLeggings() != null) {
        stand.getEquipment().setLeggings(kit.getLeggings().clone(), true);
      }
      if (kit.getBoots() != null) {
        stand.getEquipment().setBoots(kit.getBoots().clone(), true);
      }
      if (!kit.getInventory().isEmpty() && kit.getInventory().get(0) != null) {
        stand.getEquipment().setItemInMainHand(kit.getInventory().get(0));
      }
      for (EquipmentSlot slot : EquipmentSlot.values()) {
        stand.addEquipmentLock(slot, ArmorStand.LockType.REMOVING_OR_CHANGING);
      }
    });
  }

  public void deleteKit(Kit kit) {
    for (ArmorStand armorStand : this.plugin.getMapManager().getActiveWorld()
        .getEntitiesByClass(ArmorStand.class)) {
      if (armorStand.hasMetadata("ffa-kitselect")) {
        MetadataValue value = armorStand.getMetadata("ffa-kitselect").get(0);
        if (value.asString().equalsIgnoreCase(kit.getName())) {
          armorStand.remove();
        }
      }
    }
    this.plugin.getConfig().set("kits." + kit.getName().toLowerCase(), null);
    this.plugin.saveConfig();
    this.kits.remove(kit.getName().toLowerCase(), kit);
  }

  public void saveKit(Kit kit) {
    // If a section already exists we need to remove it first
    this.plugin.getConfig().set("kits." + kit.getName().toLowerCase(), null);
    ConfigurationSection config = this.plugin.getConfig()
        .createSection("kits." + kit.getName().toLowerCase());
    config.set("name", kit.getName());
    config.set("helmet", kit.getHelmet());
    config.set("chestplate", kit.getChestplate());
    config.set("leggings", kit.getLeggings());
    config.set("boots", kit.getBoots());
    config.set("armor-stand", kit.getArmorStandLocation());
    ConfigurationSection items = config.createSection("items");

    for (Map.Entry<Integer, ItemStack> inv : kit.getInventory().entrySet()) {
      items.set(inv.getKey().toString(), inv.getValue());
    }
    if (!kit.getPotionEffect().isEmpty()) {
      ConfigurationSection effects = config.createSection("effects");
      for (Map.Entry<PotionEffectType, Integer> entry : kit.getPotionEffect().entrySet()) {
        if (entry.getKey() == null) {
          continue;
        }
        effects.set(entry.getKey().getName(), entry.getValue());
      }
    }
    this.plugin.saveConfig();
    this.kits.put(kit.getName().toLowerCase(), kit);

  }

  public void applyKit(Player player, Kit kit) {
    player.getInventory().clear();
    player.getInventory().setArmorContents(null);
    player.getInventory().setHelmet(unbreakable(kit.getHelmet()));
    player.getInventory().setChestplate(unbreakable(kit.getChestplate()));
    player.getInventory().setLeggings(unbreakable(kit.getLeggings()));
    player.getInventory().setBoots(unbreakable(kit.getBoots()));
    for (Map.Entry<Integer, ItemStack> items : kit.getInventory().entrySet()) {
      player.getInventory().setItem(items.getKey(), unbreakable(items.getValue()));
    }
    for (Map.Entry<PotionEffectType, Integer> effect : kit.getPotionEffect().entrySet()) {
      player.addPotionEffect(
          new PotionEffect(effect.getKey(), Integer.MAX_VALUE,
              effect.getValue(), true, false,
              true));
    }
  }
}
