package net.venade.games.ffa_classic.kits;

import java.util.HashMap;
import java.util.TreeMap;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

/**
 * @author JakeMT04
 * @since 15/07/2021
 */
@Getter
@Setter
public class Kit {

    private TreeMap<Integer, ItemStack> inventory;
    private HashMap<PotionEffectType, Integer> potionEffect;
    private ItemStack helmet;
    private ItemStack leggings;
    private ItemStack chestplate;
    private ItemStack boots;
    private String name;
    private Location armorStandLocation;
}
