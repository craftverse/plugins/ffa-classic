package net.venade.games.ffa_classic.handler;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import lombok.AllArgsConstructor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.player.FFAPlayer;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.scoreboard.test.VenadeBoard;
import org.bukkit.Particle;
import org.bukkit.Sound;

/**
 * @author JakeMT04
 * @since 20/07/2021
 */
@AllArgsConstructor
public class ScoreboardHandler {

    private final FFAClassicPlugin plugin;

    public void registerInto(FFAPlayer player) {
        this.updateBoard(player);
    }

    private void updateBoard(FFAPlayer player) {
        if (player.getCorePlayer().getCubidPlayer() == null) {
            return;
        }
        VenadeBoard builder = player.getBoard();
        builder.updateTitle(player.getCorePlayer().getLanguageString("ffa.scoreboard.title"));
        ArrayList<String> list = new ArrayList<>(Arrays.asList(
            "",
            player.getCorePlayer().getLanguageString("ffa.scoreboard.stats.header"),
            player.getCorePlayer()
                .getLanguageString("ffa.scoreboard.stats.ranking", player.getStatsProfile()
                    .getRanking("FFA_CLASSIC_POINTS", player.getBase().getUniqueId())),
            player.getCorePlayer()
                .getLanguageString("ffa.scoreboard.stats.kills",
                    player.getStatsProfile().get("FFA_CLASSIC_KILLS")),
            player.getCorePlayer()
                .getLanguageString("ffa.scoreboard.stats.deaths",
                    player.getStatsProfile().get("FFA_CLASSIC_DEATHS")),
            player.getCorePlayer().getLanguageString("ffa.scoreboard.stats.kdr", player.getKDR()),
            player.getCorePlayer()
                .getLanguageString("ffa.scoreboard.stats.streak.current",
                    player.getCurrentStreak()),
            player.getCorePlayer()
                .getLanguageString("ffa.scoreboard.stats.streak.best",
                    player.getStatsProfile().get("FFA_CLASSIC_BEST_KILLSTREAK")),
            "",
            player.getCorePlayer().getLanguageString("ffa.scoreboard.server.header"),
            player.getCorePlayer().getLanguageString("ffa.scoreboard.server.map",
                this.plugin.getMapManager().getActiveWorld().getName()),
            player.getCorePlayer().getLanguageString("ffa.scoreboard.server.players",
                this.plugin.getServer().getOnlinePlayers().size(),
                this.plugin.getServer().getMaxPlayers()),
            player.getCorePlayer().getLanguageString("ffa.scoreboard.server.restart",
                String.format("%02d:%02d", (plugin.getRestartTime() % 3600) / 60,
                    plugin.getRestartTime() % 60)),
            "",
            player.getCorePlayer().getLanguageString("ffa.scoreboard.domain")
        ));
        builder.updateLines(list);
    }

    public void updateActionBar(FFAPlayer player) {
        if (player.getCorePlayer().getCubidPlayer() == null) {
            return;
        }
        long combatTag = player.getCombatTag();
        long spawnTime = player.getSpawnTeleport();
        long now = System.currentTimeMillis();
        boolean sendBlank = false;
        if (combatTag != 0 && combatTag >= now) {
            double val = (double) (combatTag - now) / 1000;
            double diff = new BigDecimal(val).setScale(1, RoundingMode.CEILING).doubleValue();
            TextComponent component = HexColor.translateToCompoment(
                player.getCorePlayer().getCubidPlayer()
                    .getLanguageString("ffa.actionbar.combat", String.format("%.1f", diff)));
            player.getBase().spigot().sendMessage(ChatMessageType.ACTION_BAR, component);
            player.getCorePlayer().setVanishedActionBar(false);
        } else {
            if (combatTag != 0) {
                player.getCorePlayer().sendMessage("ffa.combat.out");
                sendBlank = true;
                player.getCorePlayer().setVanishedActionBar(true);
            }
            player.setCombatTag(0);
            if (spawnTime != 0 && spawnTime >= now) {
                double val = (double) (spawnTime - now) / 1000;
                double displayVal = new BigDecimal(val).setScale(1, RoundingMode.CEILING)
                    .doubleValue();

                TextComponent component = HexColor.translateToCompoment(
                    player.getCorePlayer().getCubidPlayer()
                        .getLanguageString("ffa.actionbar.spawn",
                            String.format("%.1f", displayVal)));
                player.getBase().spigot().sendMessage(ChatMessageType.ACTION_BAR, component);
                player.getCorePlayer().setVanishedActionBar(false);
            } else {
                if (spawnTime != 0) {
                    plugin.getServer().getScheduler().runTask(plugin, () -> {
                        this.plugin.getSpawnHandler().teleportToSpawn(player);
                        player.getBase().playSound(this.plugin.getSpawnHandler().getSpawnLocation(),
                            Sound.ENTITY_ILLUSIONER_MIRROR_MOVE, 1, 1);
                        player.getBase().spawnParticle(Particle.END_ROD,
                            this.plugin.getSpawnHandler().getSpawnLocation(), 50, 0, 0.2F, 0F,
                            0.1F);
                        player.setState(FFAPlayer.State.SPAWN);
                    });

                    sendBlank = true;
                }
                player.setSpawnTeleport(0);
            }
        }
        if (sendBlank) {
            player.getBase().spigot()
                .sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(""));
            player.getCorePlayer().setVanishedActionBar(true);
        }
    }

    public void updateBoards() {
        for (FFAPlayer player : this.plugin.getPlayers().values()) {
            this.updateBoard(player);
        }
    }

    public void updateBars() {
        for (FFAPlayer player : this.plugin.getPlayers().values()) {
            this.updateActionBar(player);
        }
    }
}
