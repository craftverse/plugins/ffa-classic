package net.venade.games.ffa_classic.handler;

import net.venade.games.ffa_classic.player.FFAPlayer;
import net.venade.internal.api.inventory.item.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * @author JakeMT04
 * @since 15/07/2021
 */

public class HotbarHandler {

  private final ItemBuilder kitSelector;
  private final ItemBuilder backToHub;
  private final ItemBuilder mapRating;
  private final ItemBuilder achievements;
  private final ItemBuilder tutorial;

  public HotbarHandler() {
    this.kitSelector = ItemBuilder.builder(Material.END_CRYSTAL);
    this.backToHub = ItemBuilder.builder(Material.DRAGON_BREATH);
    this.mapRating = ItemBuilder.builder(Material.PAPER);
    this.achievements = ItemBuilder.builder(Material.NETHER_STAR);
    this.tutorial = ItemBuilder.builder(Material.KNOWLEDGE_BOOK);
  }

  public void giveTo(FFAPlayer player, boolean select) {

    player.getBase().getInventory().setItem(0, this.getKitSelector(player));
    //player.getBase().getInventory().setItem(1, this.getMapRating(player));
    //player.getBase().getInventory().setItem(6, this.getAchievements(player));
    //player.getBase().getInventory().setItem(7, this.getTutorial(player));
    player.getBase().getInventory().setItem(8, this.getBackToHub(player));
    if (select) {
      player.getBase().getInventory().setHeldItemSlot(0);
    }
  }

  public ItemStack getKitSelector(FFAPlayer player) {
    return this.kitSelector.display(
        player.getCorePlayer().getLanguageString("ffa.item.kitselector")).build();
  }

  public ItemStack getBackToHub(FFAPlayer player) {
    return this.backToHub.display(
            player.getCorePlayer().getLanguageString("ffa.item.backtohub"))
        .build();
  }

  public ItemStack getMapRating(FFAPlayer player) {
    return this.mapRating.display(
        player.getCorePlayer().getLanguageString("ffa.item.maprating")).build();
  }

  public ItemStack getAchievements(FFAPlayer player) {
    return this.achievements.display(
            player.getCorePlayer().getLanguageString("ffa.item.achievements"))
        .build();
  }

  public ItemStack getTutorial(FFAPlayer player) {
    return this.tutorial.display(player.getCorePlayer().getLanguageString("ffa.item.tutorial"))
        .build();
  }
}
