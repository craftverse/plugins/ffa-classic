package net.venade.games.ffa_classic.handler;

import lombok.AllArgsConstructor;
import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.player.FFAPlayer;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;

/**
 * @author JakeMT04
 * @since 15/07/2021
 */
@AllArgsConstructor
public class SpawnHandler {

    private final FFAClassicPlugin plugin;

    public void teleportToSpawn(FFAPlayer player) {
        player.getBase().getInventory().clear();
        player.getBase().getInventory().setArmorContents(null);
        player.getBase().getActivePotionEffects()
            .forEach(e -> player.getBase().removePotionEffect(e.getType()));
        player.getBase().setHealth(20);
        player.getBase().setFoodLevel(20);
        player.getBase().setAbsorptionAmount(0);
        player.getBase().setGameMode(GameMode.ADVENTURE);
        player.getBase().teleport(this.getSpawnLocation());
        this.plugin.getHotbarHandler().giveTo(player, true);
    }

    public Location getSpawnLocation() {
        Location spawn = this.plugin.getMapManager().getSpawnLocation();

        if (spawn == null) {
            return Bukkit.getWorld("world").getSpawnLocation();
        }

        return spawn;
    }
}
