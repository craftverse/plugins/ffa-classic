package net.venade.games.ffa_classic;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.Getter;
import net.venade.games.ffa_classic.commands.AdminCommand;
import net.venade.games.ffa_classic.commands.SpawnCommand;
import net.venade.games.ffa_classic.handler.HotbarHandler;
import net.venade.games.ffa_classic.handler.ScoreboardHandler;
import net.venade.games.ffa_classic.handler.SpawnHandler;
import net.venade.games.ffa_classic.kits.Kit;
import net.venade.games.ffa_classic.kits.KitManager;
import net.venade.games.ffa_classic.listener.HotbarListener;
import net.venade.games.ffa_classic.listener.InteractListener;
import net.venade.games.ffa_classic.listener.KillListener;
import net.venade.games.ffa_classic.listener.PlayerJoinListener;
import net.venade.games.ffa_classic.map.MapManager;
import net.venade.games.ffa_classic.map.SpawnLocation;
import net.venade.games.ffa_classic.player.FFAPlayer;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.command.Context;
import net.venade.internal.api.command.exc.CommandArgumentException;
import net.venade.internal.api.command.exc.CommandException;
import net.venade.internal.api.command.provider.Provider;
import net.venade.internal.api.command.provider.Providers;
import net.venade.internal.api.command.registration.CommandManager;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author JakeMT04
 * @since 15/07/2021
 */
@Getter
public class FFAClassicPlugin extends JavaPlugin {

    private final HashMap<UUID, FFAPlayer> players = new HashMap<>();
    private SpawnHandler spawnHandler;
    private HotbarHandler hotbarHandler;
    private KitManager kitManager;
    private MapManager mapManager;
    private ScoreboardHandler scoreboardHandler;
    private long rebootTime;
    private int restartTime = 60*60;

    public void broadcast(String key, Object... args) {
        for (FFAPlayer player : players.values()) {
            player.getCorePlayer().sendMessage(key, args);
        }
    }

    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        this.mapManager = new MapManager(this);
        this.mapManager.loadWorld();
        this.spawnHandler = new SpawnHandler(this);
        this.hotbarHandler = new HotbarHandler();
        this.kitManager = new KitManager(this);
        this.scoreboardHandler = new ScoreboardHandler(this);
        GregorianCalendar currentTime = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
        currentTime.add(Calendar.HOUR, 1);
        rebootTime = currentTime.getTimeInMillis();
        Providers.bind(PotionEffectType.class, new Provider<>() {
            @Override
            public PotionEffectType provide(Context context) throws CommandException {
                PotionEffectType type = PotionEffectType.getByName(
                    context.getArgument().toUpperCase());
                if (type == null) {
                    throw new CommandArgumentException("ffa.effect.notfound",
                        context.getArgument());
                }
                return type;
            }

            @Override
            public List<String> suggest(Context context) {
                return Arrays.stream(PotionEffectType.values())
                    .map(e -> e.getName().toUpperCase())
                    .collect(Collectors.toList());
            }
        });
        Providers.bind(Kit.class, new Provider<>() {
            @Override
            public Kit provide(Context context) throws CommandException {
                Kit kit = kitManager.getKit(context.getArgument());
                if (kit == null) {
                    throw new CommandArgumentException("ffa.kit.noexist", context.getArgument());
                }
                return kit;
            }

            @Override
            public List<String> suggest(Context context) {
                return kitManager.getKits().stream().map(Kit::getName)
                    .sorted(Comparator.comparing(String::toLowerCase)).collect(Collectors.toList());
            }
        });
        Providers.bind(SpawnLocation.class, new Provider<>() {
            @Override
            public SpawnLocation provide(Context context) throws CommandException {
                SpawnLocation location = mapManager.getSpawnLocation(context.getArgument());
                if (location == null) {
                    throw new CommandArgumentException("ffa.location.noexist",
                        context.getArgument());
                }
                return location;
            }

            @Override
            public List<String> suggest(Context context) {
                return mapManager.getSpawnLocations().stream().map(SpawnLocation::getName)
                    .sorted(Comparator.comparing(String::toLowerCase)).collect(Collectors.toList());
            }
        });
        this.getServer().getPluginManager().registerEvents(new HotbarListener(this), this);
        this.getServer().getPluginManager().registerEvents(new PlayerJoinListener(this), this);
        this.getServer().getPluginManager().registerEvents(new KillListener(this), this);
        this.getServer().getPluginManager().registerEvents(new InteractListener(this), this);
        this.getServer().getScheduler().runTaskLater(this, () -> this.getServer()
                .dispatchCommand(this.getServer().getConsoleSender(), "minecraft:reload"),
            1L); // Fix a bug in spigot
       // this.getServer().getScheduler()
       //     .runTaskTimerAsynchronously(this, this.scoreboardHandler::updateBoards, 10L, 10L);
        this.handleTimer();
        this.getServer().getScheduler()
            .runTaskTimer(this, this.scoreboardHandler::updateBars, 1L, 1L);
        CommandManager.registerCommand(new AdminCommand(this), this);
        CommandManager.registerCommand(new SpawnCommand(this), this);
    }

    //scoreboard & restart timer
    public void handleTimer() {
        new BukkitRunnable() {
            @Override
            public void run() {
                restartTime--;
                scoreboardHandler.updateBoards();

                if(restartTime > 1 && restartTime <= 10 || restartTime == 60) {
                    broadcast("ffa.restart.timer.countdown", restartTime);
                    getPlayers().forEach((uuid, ffaPlayer) -> ffaPlayer.getBase().playSound(ffaPlayer.getBase().getLocation(),
                        Sound.BLOCK_NOTE_BLOCK_PLING, 1F, 1F));
                }

                if(restartTime == 1) {
                    broadcast("ffa.restart.timer.countdown.singular", restartTime);
                    getPlayers().forEach((uuid, ffaPlayer) -> ffaPlayer.getBase().playSound(ffaPlayer.getBase().getLocation(),
                        Sound.BLOCK_NOTE_BLOCK_PLING, 1F, 1F));
                }

                if(restartTime <= 0) {
                    players.forEach((uuid, ffaPlayer) -> VenadeAPI.getStatisticsLoader().saveStats(ffaPlayer.getBase().getUniqueId()));
                    Bukkit.shutdown();
                }
            }
        }.runTaskTimerAsynchronously(this, 40L, 20L);
    }
}
