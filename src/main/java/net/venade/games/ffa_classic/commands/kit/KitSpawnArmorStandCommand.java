package net.venade.games.ffa_classic.commands.kit;

import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.kits.Kit;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.Name;
import org.bukkit.entity.Player;

/**
 * @author JakeMT04
 * @since 09/08/2021
 */
public class KitSpawnArmorStandCommand extends VenadeCommand {

    private final FFAClassicPlugin plugin;

    public KitSpawnArmorStandCommand(FFAClassicPlugin plugin) {
        super(
            "spawnarmorstand",
            "Spawns an armor stand for a kit",
            null
        );
        this.plugin = plugin;
    }

    @CommandMethod
    public void onCommand(Sender sender, @Name("name") Kit kit) {
        Player player = sender.getAsPlayer();
        kit.setArmorStandLocation(player.getLocation());
        this.plugin.getKitManager().saveKit(kit);
        this.plugin.getKitManager().generateArmorStand(kit);
        sender.sendRawMessage(
            "§fYou have spawned an armor stand for the §b" + kit.getName() + "§f kit.");
    }
}
