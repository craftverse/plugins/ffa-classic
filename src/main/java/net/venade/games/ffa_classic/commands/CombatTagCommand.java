package net.venade.games.ffa_classic.commands;

import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.player.FFAPlayer;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.Default;

/**
 * @author JakeMT04
 * @since 21/12/2021
 */
public class CombatTagCommand extends VenadeCommand {

    private final FFAClassicPlugin plugin;

    public CombatTagCommand(FFAClassicPlugin plugin) {
        super(
            "combattag",
            "does thing",
            null
        );
        this.plugin = plugin;
    }

    @CommandMethod
    public void onCommand(Sender sender, @Default("15") int seconds) {
        FFAPlayer player = this.plugin.getPlayers().get(sender.getUniqueId());
        if (player != null) {
            long now = System.currentTimeMillis();
            long fifteenSeconds = now + (seconds * 1000L);
            player.setCombatTag(fifteenSeconds);
        }
    }
}
