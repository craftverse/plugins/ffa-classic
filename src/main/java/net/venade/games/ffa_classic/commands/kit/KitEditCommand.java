package net.venade.games.ffa_classic.commands.kit;

import java.util.TreeMap;
import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.kits.Kit;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.Name;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author JakeMT04
 * @since 09/08/2021
 */
public class KitEditCommand extends VenadeCommand {

    private final FFAClassicPlugin plugin;

    public KitEditCommand(FFAClassicPlugin plugin) {
        super(
            "edit",
            "Edits the contents of a kit",
            null
        );
        this.plugin = plugin;
    }

    @CommandMethod
    public void onCommand(Sender sender, @Name("name") Kit kit) {
        if (sender.isConsole()) {
            return;
        }
        Player player = sender.getAsPlayer();
        if (player.getInventory().getHelmet() != null) {
            kit.setHelmet(player.getInventory().getHelmet().clone());
        }
        if (player.getInventory().getChestplate() != null) {
            kit.setChestplate(player.getInventory().getChestplate().clone());
        }
        if (player.getInventory().getLeggings() != null) {
            kit.setLeggings(player.getInventory().getLeggings().clone());
        }
        if (player.getInventory().getBoots() != null) {
            kit.setBoots(player.getInventory().getBoots().clone());
        }
        int i = 0;
        TreeMap<Integer, ItemStack> items = new TreeMap<>();
        for (ItemStack item : player.getInventory().getStorageContents()) {
            if (item == null) {
                i++;
                continue;
            }
            items.put(i, item.clone());
            i++;
        }
        kit.setInventory(items);
        this.plugin.getKitManager().saveKit(kit);
        sender.sendRawMessage(
            "§fYou have updated the kit §b" + kit.getName() + "§f with new contents.");
    }
}
