package net.venade.games.ffa_classic.commands.kit;

import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.kits.Kit;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.Default;
import org.bukkit.potion.PotionEffectType;

/**
 * @author JakeMT04
 * @since 05/12/2021
 */
public class KitAddPotionEffectCommand extends VenadeCommand {

    private final FFAClassicPlugin plugin;

    public KitAddPotionEffectCommand(FFAClassicPlugin plugin) {
        super("setpotioneffect", "Sets the potion effects that are given", null);
        this.plugin = plugin;
    }

    @CommandMethod
    public void onCommand(Sender sender, Kit kit, PotionEffectType effect,
        @Default("1") int level) {
        if (level <= 0) {
            // remove
            kit.getPotionEffect().remove(effect);
            sender.sendRawMessage(
                "You have removed potion effect §b" + effect.getName() + "§f from the kit.");
        } else {
            kit.getPotionEffect().put(effect, level - 1);
            sender.sendRawMessage(
                "You have added potion effect §b" + effect.getName() + "§f at level §b" + level +
                    "§fto the kit.");
        }
        this.plugin.getKitManager().saveKit(kit);
    }

}
