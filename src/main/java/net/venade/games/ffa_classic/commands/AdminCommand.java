package net.venade.games.ffa_classic.commands;

import cubid.cloud.modules.player.permission.CubidRank;
import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.commands.kit.KitCommand;
import net.venade.games.ffa_classic.commands.spawnloc.SpawnLocationCommand;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.permission.Permission;

/**
 * @author JakeMT04
 * @since 09/08/2021
 */
public class AdminCommand extends VenadeCommand {

    public AdminCommand(FFAClassicPlugin plugin) {
        super(
            "admin",
            "Admin management commands",
            Permission.has(CubidRank.ADMIN),
            true
        );
        registerSubCommand(new SpawnLocationCommand(plugin));
        registerSubCommand(new KitCommand(plugin));
        registerSubCommand(new ReloadConfiguration(plugin));
        registerSubCommand(new SetHologramsLocationCommand(plugin));
        registerSubCommand(new CombatTagCommand(plugin));
        registerSubCommand(new SetRankingHologramsLocationCommand(plugin));
        registerSubCommand(new SetSpawnLocationCommand(plugin));
    }
}
