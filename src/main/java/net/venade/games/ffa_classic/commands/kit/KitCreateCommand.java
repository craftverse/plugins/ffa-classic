package net.venade.games.ffa_classic.commands.kit;

import java.util.HashMap;
import java.util.TreeMap;
import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.kits.Kit;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author JakeMT04
 * @since 09/08/2021
 */
public class KitCreateCommand extends VenadeCommand {

    private final FFAClassicPlugin plugin;

    public KitCreateCommand(FFAClassicPlugin plugin) {
        super(
            "create",
            "Creates a new kit",
            null
        );
        this.plugin = plugin;
    }

    @CommandMethod
    public void onCommand(Sender sender, String name) {
        if (sender.isConsole()) {
            return;
        }
        Player player = sender.getAsPlayer();
        if (this.plugin.getKitManager().kitExists(name)) {
            sender.sendRawMessage("§cA kit with that name already exists.");
            return;
        }
        Kit kit = new Kit();
        kit.setName(name);
        if (player.getInventory().getHelmet() != null) {
            kit.setHelmet(player.getInventory().getHelmet().clone());
        }
        if (player.getInventory().getChestplate() != null) {
            kit.setChestplate(player.getInventory().getChestplate().clone());
        }
        if (player.getInventory().getLeggings() != null) {
            kit.setLeggings(player.getInventory().getLeggings().clone());
        }
        if (player.getInventory().getBoots() != null) {
            kit.setBoots(player.getInventory().getBoots().clone());
        }
        int i = 0;
        TreeMap<Integer, ItemStack> items = new TreeMap<>();
        for (ItemStack item : player.getInventory().getStorageContents()) {
            if (item == null) {
                i++;
                continue;
            }
            items.put(i, item.clone());
            i++;
        }
        kit.setInventory(items);
        kit.setPotionEffect(new HashMap<>());
        this.plugin.getKitManager().saveKit(kit);
        sender.sendRawMessage("You have created a new kit with name §b" + name + "§f.");
    }

}
