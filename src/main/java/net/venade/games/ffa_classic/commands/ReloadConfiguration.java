package net.venade.games.ffa_classic.commands;

import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;

/**
 * @author JakeMT04
 * @since 03/08/2021
 */
public class ReloadConfiguration extends VenadeCommand {

    private final FFAClassicPlugin plugin;

    public ReloadConfiguration(FFAClassicPlugin plugin) {
        super(
            "reload",
            "Reloads all of the configuration files",
            null
        );
        this.plugin = plugin;
    }

    @CommandMethod
    public void onCommand(Sender sender) {
        this.plugin.reloadConfig();
        sender.sendRawMessage("Configuration files reloaded! §7[config.yml]");
    }
}
