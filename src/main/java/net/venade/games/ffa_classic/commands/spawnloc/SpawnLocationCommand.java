package net.venade.games.ffa_classic.commands.spawnloc;

import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.internal.api.command.VenadeCommand;

/**
 * @author JakeMT04
 * @since 09/08/2021
 */
public class SpawnLocationCommand extends VenadeCommand {

    public SpawnLocationCommand(FFAClassicPlugin plugin) {
        super(
            "spawnlocation",
            "Manage the spawn locations",
            null,
            true
        );
        registerSubCommand(new AddSpawnLocationCommand(plugin));
        registerSubCommand(new RemoveSpawnLocationCommand(plugin));

    }
}
