package net.venade.games.ffa_classic.commands.kit;

import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.internal.api.command.VenadeCommand;

/**
 * @author JakeMT04
 * @since 09/08/2021
 */
public class KitCommand extends VenadeCommand {

    public KitCommand(FFAClassicPlugin plugin) {
        super(
            "kit",
            "Manage the kits",
            null,
            true
        );
        registerSubCommand(new KitCreateCommand(plugin));
        registerSubCommand(new KitSpawnArmorStandCommand(plugin));
        registerSubCommand(new KitDeleteCommand(plugin));
        registerSubCommand(new KitEditCommand(plugin));
        registerSubCommand(new KitAddPotionEffectCommand(plugin));
    }
}
