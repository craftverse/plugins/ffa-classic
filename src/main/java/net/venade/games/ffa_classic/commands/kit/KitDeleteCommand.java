package net.venade.games.ffa_classic.commands.kit;

import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.kits.Kit;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.Name;

/**
 * @author JakeMT04
 * @since 09/08/2021
 */
public class KitDeleteCommand extends VenadeCommand {

    private final FFAClassicPlugin plugin;

    public KitDeleteCommand(FFAClassicPlugin plugin) {
        super(
            "delete",
            "Deletes a kit",
            null
        );
        this.plugin = plugin;
    }

    @CommandMethod
    public void onCommand(Sender sender, @Name("name") Kit kit) {
        this.plugin.getKitManager().deleteKit(kit);
        sender.sendRawMessage("§fYou have deleted the kit §b" + kit.getName() + "§f.");
    }
}
