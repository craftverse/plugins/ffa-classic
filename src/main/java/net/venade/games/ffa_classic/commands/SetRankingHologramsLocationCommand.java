package net.venade.games.ffa_classic.commands;

import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.player.FFAPlayer;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import org.bukkit.entity.Player;

/**
 * @author JakeMT04
 * @since 05/12/2021
 */
public class SetRankingHologramsLocationCommand extends VenadeCommand {

    private final FFAClassicPlugin plugin;

    public SetRankingHologramsLocationCommand(FFAClassicPlugin plugin) {
        super("setrankingholo", "Set the hologram spawn location pf the top 10 list", null);
        this.plugin = plugin;
    }

    @CommandMethod
    public void onCommand(Sender sender) {
        if (sender.isConsole()) {
            return;
        }
        Player player = sender.getAsPlayer();
        this.plugin.getConfig().set("lobby-holograms-ranking", player.getLocation().clone());
        this.plugin.saveConfig();

        for (FFAPlayer player1 : this.plugin.getPlayers().values()) {
            player1.spawnHolograms();
        }
    }
}
