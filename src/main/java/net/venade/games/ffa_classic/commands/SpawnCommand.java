package net.venade.games.ffa_classic.commands;

import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.player.FFAPlayer;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * @author JakeMT04
 * @since 09/08/2021
 */
public class SpawnCommand extends VenadeCommand {

    private final FFAClassicPlugin plugin;

    public SpawnCommand(FFAClassicPlugin plugin) {
        super(
            "spawn",
            "Teleports you to spawn",
            null
        );
        this.plugin = plugin;
    }

    @CommandMethod
    public void onCommand(Sender sender) {
        if (sender.isConsole()) {
            return;
        }
        Player player = sender.getAsPlayer();

        FFAPlayer ffaPlayer = plugin.getPlayers().get(player.getUniqueId());
        if (ffaPlayer == null) {
            player.kickPlayer("You aren't registered!");
            return;
        }
        if (ffaPlayer.getState() == FFAPlayer.State.SPAWN) {
            sender.sendMessage("ffa.spawn.already.in");
            return;
        }
        if (ffaPlayer.getSpawnTeleport() != 0) {
            sender.sendMessage("ffa.spawn.already.teleporting");
            return;
        }
        if (ffaPlayer.getCombatTag() != 0
            && ffaPlayer.getCombatTag() >= System.currentTimeMillis()) {
            sender.sendMessage("ffa.spawn.incombat");
            return;
        }
        ffaPlayer.setSpawnTeleport(System.currentTimeMillis() + 5000);
        sender.sendMessage("ffa.spawn.teleporting", 5);
        ffaPlayer.getBase()
            .playSound(ffaPlayer.getBase().getLocation(), Sound.BLOCK_NOTE_BLOCK_IRON_XYLOPHONE, 1,
                1);
    }
}
