package net.venade.games.ffa_classic.commands.spawnloc;

import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

/**
 * @author JakeMT04
 * @since 26/07/2021
 */
public class AddSpawnLocationCommand extends VenadeCommand {

    private final FFAClassicPlugin plugin;

    public AddSpawnLocationCommand(FFAClassicPlugin plugin) {
        super(
            "add",
            "Adds a spawn location",
            null
        );
        this.plugin = plugin;
    }

    @CommandMethod
    public void onCommand(Sender sender, String name) {
        Player player = sender.getAsPlayer();
        Location loc = player.getLocation();
        if (player.getInventory().getItemInMainHand().getType() == Material.AIR) {
            sender.sendRawMessage(
                "§cYou need to hold an item in your hand to set the display item for this location!");
            return;
        }
        Material mat = player.getInventory().getItemInMainHand().getType();
        ConfigurationSection parent = this.plugin.getConfig()
            .getConfigurationSection("locations." + loc.getWorld().getName());
        if (parent == null) {
            parent = this.plugin.getConfig().createSection("locations." + loc.getWorld().getName());
        }
        ConfigurationSection config = parent.getConfigurationSection(name.toLowerCase());
        if (config == null) {
            config = parent.createSection(name.toLowerCase());
        } else {
            sender.sendRawMessage("§cA spawn location with name §f" + name + "§c already exists.");
            return;
        }
        config.set("location", loc);
        config.set("icon", mat.name());
        config.set("name", name);
        this.plugin.saveConfig();
        sender.sendRawMessage(
            "§fYou have added a new spawn location with name §b" + name + "§f and display item §b"
                + mat.name() + "§f.");
    }
}
