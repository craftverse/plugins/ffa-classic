package net.venade.games.ffa_classic.commands.spawnloc;

import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.map.SpawnLocation;
import net.venade.internal.api.command.Sender;
import net.venade.internal.api.command.VenadeCommand;
import net.venade.internal.api.command.annotation.CommandMethod;
import net.venade.internal.api.command.annotation.Name;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

/**
 * @author JakeMT04
 * @since 26/07/2021
 */
public class RemoveSpawnLocationCommand extends VenadeCommand {

    private final FFAClassicPlugin plugin;

    public RemoveSpawnLocationCommand(FFAClassicPlugin plugin) {
        super(
            "remove",
            "Removes a spawn location",
            null
        );
        this.plugin = plugin;
    }

    @CommandMethod
    public void onCommand(Sender sender, @Name("name") SpawnLocation location) {
        Location loc = location.getLocation();
        ConfigurationSection parent = this.plugin.getConfig()
            .getConfigurationSection("locations." + loc.getWorld().getName());
        if (parent == null) {
            return;
        }
        parent.set(location.getName().toLowerCase(), null);
        this.plugin.saveConfig();
        sender.sendRawMessage(
            "§fYou have removed the spawn location with name §b" + location.getName() + "§f.");
    }
}
