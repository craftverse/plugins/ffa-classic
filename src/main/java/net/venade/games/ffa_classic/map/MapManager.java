package net.venade.games.ffa_classic.map;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.AllArgsConstructor;
import net.venade.games.ffa_classic.FFAClassicPlugin;
import org.bukkit.GameRule;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.ConfigurationSection;

/**
 * @author JakeMT04
 * @since 16/07/2021
 */
@AllArgsConstructor
public class MapManager {

    private final FFAClassicPlugin plugin;

    public World getActiveWorld() {
        return this.plugin.getServer().getWorld(this.getWorldName());
    }

    public void loadWorld() {
        World world = this.plugin.getServer().createWorld(new WorldCreator(this.getWorldName()));
        if (world != null) {
            world.setStorm(false);
            world.setThundering(false);
            world.setGameRule(GameRule.DO_IMMEDIATE_RESPAWN, false);
            world.setGameRule(GameRule.FALL_DAMAGE, false);
            world.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
        }
    }

    private String getWorldName() {
        return this.plugin.getConfig().getString("world-name", "world");
    }

    public SpawnLocation getSpawnLocation(String name) {
        ConfigurationSection config = this.plugin.getConfig()
            .getConfigurationSection("locations." + this.getWorldName());
        if (config == null) {
            return null;
        }
        ConfigurationSection loc = config.getConfigurationSection(name.toLowerCase());
        if (loc == null) {
            return null;
        }
        Location location = loc.getLocation("location");
        String nm = loc.getString("name");
        Material item = Material.valueOf(loc.getString("icon"));
        return new SpawnLocation(nm, location, item);

    }

    public Location getLobbyHologramLocation() {
        return this.plugin.getConfig().getLocation("lobby-holograms");
    }

    public Location getRankingologramLocation() {
        return this.plugin.getConfig().getLocation("lobby-holograms-ranking");
    }

    public Location getSpawnLocation() {
        return this.plugin.getConfig().getLocation("lobby-spawn");
    }

    public List<SpawnLocation> getSpawnLocations() {
        ConfigurationSection config = this.plugin.getConfig()
            .getConfigurationSection("locations." + this.getWorldName());
        if (config == null) {
            return Collections.emptyList();
        }
        List<SpawnLocation> locs = new ArrayList<>();
        for (String key : config.getKeys(false)) {
            ConfigurationSection loc = config.getConfigurationSection(key);
            if (loc == null) {
                continue;
            }
            Location location = loc.getLocation("location");
            String name = loc.getString("name");
            Material item = Material.valueOf(loc.getString("icon"));
            locs.add(new SpawnLocation(name, location, item));
        }
        return locs;
    }
}
