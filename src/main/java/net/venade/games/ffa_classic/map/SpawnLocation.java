package net.venade.games.ffa_classic.map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.Material;

/**
 * @author JakeMT04
 * @since 16/07/2021
 */
@AllArgsConstructor
@Getter
public class SpawnLocation {

    private final String name;
    private final Location location;
    private final Material icon;
}
