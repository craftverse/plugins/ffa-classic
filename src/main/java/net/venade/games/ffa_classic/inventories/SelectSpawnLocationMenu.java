package net.venade.games.ffa_classic.inventories;

import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.kits.Kit;
import net.venade.games.ffa_classic.map.SpawnLocation;
import net.venade.games.ffa_classic.player.FFAPlayer;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.inventory.ClickableItem;
import net.venade.internal.api.inventory.SmartInventory;
import net.venade.internal.api.inventory.content.InventoryContents;
import net.venade.internal.api.inventory.content.InventoryProvider;
import net.venade.internal.api.inventory.item.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author JakeMT04
 * @since 16/07/2021
 */
public class SelectSpawnLocationMenu implements InventoryProvider {

    private final FFAClassicPlugin plugin;
    private final Kit kit;
    private final FFAPlayer ffaPlayer;
    private final SmartInventory inventory;

    public SelectSpawnLocationMenu(FFAClassicPlugin plugin, FFAPlayer owner, Kit kit) {
        int rowsRequired =
            ((int) Math.ceil((float) plugin.getMapManager().getSpawnLocations().size() / 4) * 2)
                + 1;
        this.inventory = SmartInventory.builder()
            .closeable(true)
            .id("spawn-location")
            .manager(VenadeAPI.getInventoryManager())
            .provider(this)
            .title("Select spawn location")
            .size(rowsRequired, 9)
            .type(InventoryType.CHEST)
            .build();
        this.kit = kit;
        this.plugin = plugin;
        this.ffaPlayer = owner;
    }

    public void openInventory(Player player) {
        this.inventory.open(player);
    }

    public void closeInventory(Player player) {
        this.inventory.close(player);
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        int row = 1;
        int column = 1;
        for (SpawnLocation location : plugin.getMapManager().getSpawnLocations()) {
            ItemStack item = ItemBuilder.builder(location.getIcon())
                .display(ChatColor.AQUA + location.getName()).build();
            ItemMeta meta = item.getItemMeta();
            assert meta != null;
            meta.addItemFlags(ItemFlag.values());
            item.setItemMeta(meta);
            ClickableItem clickableItem = ClickableItem.of(item, (event) -> {
                this.closeInventory(player);
                this.ffaPlayer.teleportInWith(kit, location);
            });
            contents.set(row, column, clickableItem);
            if (column >= 7) {
                column = 1;
                row += 2;
            } else {
                column += 2;
            }
        }
    }

    @Override
    public void update(Player player, InventoryContents inventoryContents) {
    }
}
