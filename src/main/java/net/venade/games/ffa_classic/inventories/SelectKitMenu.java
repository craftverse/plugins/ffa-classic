package net.venade.games.ffa_classic.inventories;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.kits.Kit;
import net.venade.games.ffa_classic.map.SpawnLocation;
import net.venade.games.ffa_classic.player.FFAPlayer;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.inventory.ClickableItem;
import net.venade.internal.api.inventory.SmartInventory;
import net.venade.internal.api.inventory.content.InventoryContents;
import net.venade.internal.api.inventory.content.InventoryProvider;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_19_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * @author JakeMT04
 * @since 15/07/2021
 */
public class SelectKitMenu implements InventoryProvider {

    private final SmartInventory inventory;
    private final FFAClassicPlugin plugin;
    private final FFAPlayer ffaPlayer;

    public SelectKitMenu(FFAClassicPlugin plugin, FFAPlayer player) {
        int rowsRequired =
            ((int) Math.ceil((float) plugin.getKitManager().getKits().size() / 4) * 2) + 1;
        this.inventory = SmartInventory.builder()
            .manager(VenadeAPI.getInventoryManager())
            .provider(this)
            .id("select-kit")
            .title("Kits")
            .closeable(true)
            .size(rowsRequired, 9)
            .type(InventoryType.CHEST)
            .build();
        this.plugin = plugin;
        this.ffaPlayer = player;

    }

    public void openInventory(Player player) {
        player.playSound(player.getLocation(), Sound.ENTITY_ENDER_EYE_DEATH, 1, 1);
        this.inventory.open(player);
    }

    public void closeInventory(Player player) {
        this.inventory.close(player);
    }


    @Override
    public void init(Player player, InventoryContents contents) {
        int row = 1;
        int column = 1;

        for (Kit kit : this.plugin.getKitManager().getKits()) {
            ItemStack mainHand = kit.getInventory().get(0); // use this for now
            ItemStack item = mainHand.clone();
            ItemMeta meta = item.getItemMeta();
            if (meta == null) {
                throw new IllegalStateException("Item has no meta!");
            }
            meta.setDisplayName("§b§l" + kit.getName());
            meta.addItemFlags(ItemFlag.values());
            List<String> lore = new ArrayList<>();
            lore.add("&#d8d8d8This kit includes:");
            if (kit.getHelmet() != null) {
                ItemStack itemStack = kit.getHelmet();
                if (itemStack.getAmount() == 1) {
                    lore.add("&#e8f5ff • " + WordUtils.capitalizeFully(
                        itemStack.getType().name().replace("_", " ")));
                } else {
                    lore.add(
                        "&#e8f5ff • " + itemStack.getAmount() + "x" + WordUtils.capitalizeFully(
                            itemStack.getType().name().replace("_", " ")));
                }
            }
            if (kit.getChestplate() != null) {
                ItemStack itemStack = kit.getChestplate();
                if (itemStack.getAmount() == 1) {
                    lore.add("&#e8f5ff • " + WordUtils.capitalizeFully(
                        itemStack.getType().name().replace("_", " ")));
                } else {
                    lore.add(
                        "&#e8f5ff • " + itemStack.getAmount() + "x" + WordUtils.capitalizeFully(
                            itemStack.getType().name().replace("_", " ")));
                }
            }
            if (kit.getLeggings() != null) {
                ItemStack itemStack = kit.getLeggings();
                if (itemStack.getAmount() == 1) {
                    lore.add("&#e8f5ff • " + WordUtils.capitalizeFully(
                        itemStack.getType().name().replace("_", " ")));
                } else {
                    lore.add(
                        "&#e8f5ff • " + itemStack.getAmount() + "x" + WordUtils.capitalizeFully(
                            itemStack.getType().name().replace("_", " ")));
                }
            }
            if (kit.getBoots() != null) {
                ItemStack itemStack = kit.getBoots();
                if (itemStack.getAmount() == 1) {
                    lore.add("&#e8f5ff • " + WordUtils.capitalizeFully(
                        itemStack.getType().name().replace("_", " ")));
                } else {
                    lore.add(
                        "&#e8f5ff • " + itemStack.getAmount() + "x" + WordUtils.capitalizeFully(
                            itemStack.getType().name().replace("_", " ")));
                }
            }
            LinkedHashMap<String, AtomicInteger> things = new LinkedHashMap<>();
            for (ItemStack itemStack : kit.getInventory().values()) {
                if (itemStack.getType() == Material.AIR) {
                    continue;
                }
                String itemName = getItemName(itemStack);
                if (things.containsKey(itemName)) {
                    things.get(itemName).getAndAdd(itemStack.getAmount());
                } else {
                    things.put(itemName, new AtomicInteger(itemStack.getAmount()));
                }
            }
            for (Map.Entry<String, AtomicInteger> entry : things.entrySet()) {
                if (entry.getValue().get() == 1) {
                    lore.add("&#e8f5ff • " + entry.getKey());
                } else {
                    lore.add("&#e8f5ff • " + entry.getValue().get() + "x " + entry.getKey());
                }
            }
            lore.add("");
            lore.add("&#55eefdLeft click &#d8d8d8to randomly teleport.");
            lore.add("&#55eefdRight click &#d8d8d8to select a teleport location.");

            lore.replaceAll(HexColor::translate);
            meta.setLore(lore);
            item.setItemMeta(meta);
            ClickableItem clickableItem = ClickableItem.of(item, (event) -> {
                if (event.getClick() == ClickType.LEFT) {
                    SecureRandom random = new SecureRandom();
                    int position = random.nextInt(
                        plugin.getMapManager().getSpawnLocations().size());
                    System.out.println(position);
                    SpawnLocation location = plugin.getMapManager().getSpawnLocations()
                        .get(position);
                    this.ffaPlayer.teleportInWith(kit, location);
                } else if (event.getClick() == ClickType.RIGHT) {
                    player.playSound(player.getLocation(), Sound.ITEM_BOOK_PAGE_TURN, 1, 1);
                    new SelectSpawnLocationMenu(plugin, this.ffaPlayer, kit).openInventory(player);
                }
            });
            contents.set(row, column, clickableItem);
            if (column >= 7) {
                column = 1;
                row += 2;
            } else {
                column += 2;
            }
        }
    }

    public String getItemName(ItemStack stack) {
        net.minecraft.world.item.ItemStack nmsStack = CraftItemStack.asNMSCopy(stack);
        return nmsStack.getDisplayName().getString();
    }

    @Override
    public void update(Player player, InventoryContents inventoryContents) {

    }
}
