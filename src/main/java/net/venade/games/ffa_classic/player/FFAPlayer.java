package net.venade.games.ffa_classic.player;

import cloud.cubid.bridge.CubidCloud;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.UUID;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.kits.Kit;
import net.venade.games.ffa_classic.map.SpawnLocation;
import net.venade.internal.api.VenadeAPI;
import net.venade.internal.api.color.HexColor;
import net.venade.internal.api.entities.hologram.Hologram;
import net.venade.internal.api.player.ICorePlayer;
import net.venade.internal.api.scoreboard.test.VenadeBoard;
import net.venade.internal.api.statistics.StatsProfile;
import org.bukkit.GameMode;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @author JakeMT04
 * @since 15/07/2021
 */
@RequiredArgsConstructor
@Getter
@Setter
public class FFAPlayer {

    private final FFAClassicPlugin plugin;
    private final Player base;

    private final VenadeBoard board;
    private State state = State.SPAWN;
    private int currentStreak = 0;

    private long combatTag = 0;
    private long spawnTeleport = 0;
    private long leftSpawn = 0;
    private long hubTeleport = 0;
    private ICorePlayer corePlayer;
    private Hologram hologram, rankingHologram;
    private StatsProfile statsProfile;

    public void addKill() {
        this.currentStreak++;
        if (this.currentStreak >= statsProfile.get("FFA_CLASSIC_BEST_KILLSTREAK")) {
            statsProfile.set("FFA_CLASSIC_BEST_KILLSTREAK", this.currentStreak);
        }
        corePlayer.loadStatsProfile().add("FFA_CLASSIC_KILLS");
        statsProfile.add("FFA_CLASSIC_POINTS", 5);
        this.corePlayer.sendMessage("ffa.kill.points", 5);
        VenadeAPI.getStatisticsLoader().saveStats(getBase().getUniqueId());//Save for Ranking
        getBase().addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 5, 2, true));
    }

    public void spawnHolograms() {
        if (this.plugin.getMapManager().getLobbyHologramLocation() == null) {
            return;
        }
        if (hologram != null) {
            hologram.hidePlayer(this.getBase());
        }
        if(rankingHologram != null){
            rankingHologram.hidePlayer(this.getBase());
        }

        String[] lines = this.getCorePlayer().getLanguageArray("ffa.spawn.hologram");
        hologram = new Hologram(lines, this.plugin.getMapManager().getLobbyHologramLocation());

        LinkedList<String> top10 = VenadeAPI.getStatisticsLoader().getBestPlayersInRanking("FFA_CLASSIC_POINTS", 5);
        for (String s : top10) {
            System.out.println("TOP10: "+ s);
        }
        LinkedList<String> rankingLines = new LinkedList<>();
        rankingLines.add(this.getCorePlayer().getLanguageString("ffa.leaderboard.title"));
        rankingLines.add("");

        for (int i = 0; i < top10.size(); i++) {
            int pos = i;
            System.out.println("test" + pos);
            CubidCloud.getBridge().getPlayerManager().getOfflinePlayer(UUID.fromString(top10.get(i))).ifPresent(iOfflineCubidPlayer -> {
                System.out.println(iOfflineCubidPlayer.getName() +" ID; " + iOfflineCubidPlayer.getUniqueId());
                StatsProfile statsProfile = VenadeAPI.getStatisticsLoader().loadStats(UUID.fromString(top10.get(pos)));

                String color = "&#d8d8d8";

                int finalPos = pos+1; //Because java counts at 0
                if(finalPos == 1) {
                    color = "&#ffd700";
                }
                if(finalPos == 2) {
                    color = "&#c0c0c0";
                }
                if(finalPos == 3) {
                    color = "&#bf8970";
                }

                String rankingLine = color + "#" + finalPos + " " + iOfflineCubidPlayer.getRank().getColor() + iOfflineCubidPlayer.getName() + " &#d8d8d8» &#55eefd" + statsProfile.get("FFA_CLASSIC_POINTS");
                System.out.println(rankingLine);
                rankingLines.add(rankingLine);
            });
        }
        String[] topPlayers = rankingLines.toArray(new String[rankingLines.size()]);
        rankingHologram = new Hologram(topPlayers ,this.plugin.getMapManager().getRankingologramLocation());

        //hologram.create();
        hologram.showPlayer(this.getBase());
        rankingHologram.showPlayer(this.getBase());
    }

    public void teleportInWith(Kit kit, SpawnLocation location) {
        this.plugin.getKitManager().applyKit(this.getBase(), kit);
        this.getBase().teleport(location.getLocation());
        this.getBase().playSound(location.getLocation(), Sound.ENTITY_ILLUSIONER_MIRROR_MOVE, 1, 1);
        this.getBase().spawnParticle(Particle.END_ROD, location.getLocation(), 50, 0, 0.2F, 0F, 0.1F);
        this.setState(FFAPlayer.State.FIGHTING);
        this.setLeftSpawn(System.currentTimeMillis());

        this.plugin.getServer().getScheduler().runTaskLater(this.plugin, () -> {
            this.getBase().setGlowing(true);
        }, 5L);
        this.plugin.getServer().getScheduler().runTaskLater(this.plugin, () -> {
            this.getBase().setGlowing(false);
        }, (2 * 20L) + 5);
    }

    public ICorePlayer getCorePlayer() {
        if (this.corePlayer != null) {
            return this.corePlayer;
        }
        this.corePlayer = VenadeAPI.getPlayer(this.base.getUniqueId());
        return this.corePlayer;
    }

    public StatsProfile getStatsProfile() {
        if (this.statsProfile == null) {
            this.statsProfile = getCorePlayer().loadStatsProfile();
        }
        return statsProfile;
    }

    public void addDeath() {
        statsProfile.add("FFA_CLASSIC_DEATHS");
        statsProfile.remove(
            "FFA_CLASSIC_POINTS",
            3,
            false
        );
        this.currentStreak = 0;
        this.corePlayer.sendMessage("ffa.death.points", 3);
        VenadeAPI.getStatisticsLoader().saveStats(getBase().getUniqueId());//Save for Ranking
    }

    public double getKDR() {
        return getStatsProfile().getRate(getStatsProfile().get("FFA_CLASSIC_KILLS"),
            getStatsProfile().get("FFA_CLASSIC_DEATHS"));
    }

    public void setCombatTag(long tag) {
        this.combatTag = tag;
        if(tag < 10) {
            getBase().setGameMode(GameMode.ADVENTURE);
        }else getBase().setGameMode(GameMode.SURVIVAL);
    }

    public enum State {
        SPAWN,
        FIGHTING
    }
}
