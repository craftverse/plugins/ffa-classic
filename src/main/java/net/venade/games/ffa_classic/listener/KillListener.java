package net.venade.games.ffa_classic.listener;

import cubid.cloud.modules.player.permission.CubidRank;
import java.text.DecimalFormat;
import lombok.AllArgsConstructor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.player.FFAPlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

/**
 * @author JakeMT04
 * @since 16/07/2021
 */
@AllArgsConstructor
public class KillListener implements Listener {

  private final FFAClassicPlugin plugin;

  @EventHandler
  public void onDeath(PlayerDeathEvent event) {
    event.getDrops().clear();
    event.setDeathMessage(null);
    event.getEntity()
        .setBedSpawnLocation(this.plugin.getSpawnHandler().getSpawnLocation(), true);
    Player killer = event.getEntity().getKiller();
    FFAPlayer player = this.plugin.getPlayers().get(event.getEntity().getUniqueId());
    CubidRank rank = player.getCorePlayer().getCubidPlayer().getRank();
    if (killer != null) {
      FFAPlayer killerProfile = this.plugin.getPlayers().get(killer.getUniqueId());
      CubidRank killerRank = killerProfile.getCorePlayer().getCubidPlayer().getRank();
      this.plugin.broadcast("ffa.kill", rank.getColor() + event.getEntity().getName(),
          killerRank.getColor() + killer.getName(), new DecimalFormat(".0").format(killer.getHealth()/2));

      killerProfile.addKill();
      if (killerProfile.getCurrentStreak() % 5 == 0) {
        this.plugin.broadcast("ffa.killstreak.broadcast",
            killerRank.getColor() + killer.getName(),
            killerProfile.getCurrentStreak());
      }
      killerProfile.setCombatTag(0);
      killer.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(""));
      killer.playSound(killer.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 0.9f);
    } else {
      this.plugin.broadcast("ffa.died", rank.getColor() + event.getEntity().getName());
    }
    player.setState(FFAPlayer.State.SPAWN);
    int killstreak = player.getCurrentStreak();
    if (killstreak >= 5) {
      this.plugin.broadcast("ffa.killstreak.dead",
          rank.getColor() + event.getEntity().getName(),
          killstreak);
    }
    player.addDeath();
    player.setCombatTag(0);
    player.getBase().spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(""));
    event.getEntity().getInventory().clear();
    event.getEntity().getInventory().setArmorContents(null);
    event.getEntity()
        .playSound(event.getEntity().getLocation(), Sound.BLOCK_RESPAWN_ANCHOR_DEPLETE, 1, 1);
    this.plugin.getServer().getScheduler().runTaskLater(this.plugin, () -> {
      event.getEntity().spigot().respawn();
    }, 15L);
  }


  @EventHandler
  public void onRespawn(PlayerRespawnEvent event) {
    FFAPlayer player = this.plugin.getPlayers().get(event.getPlayer().getUniqueId());
    if (player == null) {
      return;
    }
    this.plugin.getHotbarHandler().giveTo(player, true);

  }

  @EventHandler
  public void onDamage(EntityDamageEvent event) {
    if (event.getEntity().hasMetadata("ffa-kitselect")) {
      event.setCancelled(true);
    }
  }

  private Player getPlayer(Entity entity) {
    if (entity instanceof Player) {
      return (Player) entity;
    } else if (entity instanceof Projectile) {
      Projectile proj = (Projectile) entity;
      if (proj.getShooter() instanceof Player) {
        return (Player) proj.getShooter();
      }
    }
    return null;

  }

  @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
  public void onDamageHighest(EntityDamageByEntityEvent event) {
    long now = System.currentTimeMillis();
    long fifteenSeconds = now + 15000;
    if (event.getEntity() instanceof Player && (event.getDamager() instanceof Player
        || event.getDamager() instanceof Projectile)) {
      Player damager = this.getPlayer(event.getDamager());
      if (damager == null) {
        return;
      }
      Player damagee = (Player) event.getEntity();
      if (damagee.getUniqueId().equals(damager.getUniqueId())) {
        return;
      }
      FFAPlayer damagerData = this.plugin.getPlayers().get(damager.getUniqueId());
      FFAPlayer damageeData = this.plugin.getPlayers().get(damagee.getUniqueId());
      if (damagerData.getCombatTag() == 0
          || damagerData.getCombatTag() < System.currentTimeMillis()) {
        damagerData.getCorePlayer().sendMessage("ffa.combat.in");
      }
      if (damageeData.getCombatTag() == 0
          || damageeData.getCombatTag() < System.currentTimeMillis()) {
        damageeData.getCorePlayer().sendMessage("ffa.combat.in");
      }
      damagerData.setCombatTag(fifteenSeconds);
      damageeData.setCombatTag(fifteenSeconds);
    }
  }

  @EventHandler
  public void onDamage(EntityDamageByEntityEvent event) {
    if (event.getEntity().hasMetadata("ffa-kitselect")) {
      event.setCancelled(true);
      return;
    }
    Player damager = this.getPlayer(event.getDamager());

    if (damager != null) {
      FFAPlayer data = this.plugin.getPlayers().get(damager.getUniqueId());
      if (data != null) {
        if (data.getState() == FFAPlayer.State.SPAWN) {
          event.setCancelled(true);
         // data.getCorePlayer().sendMessage("ffa.combat.spawn");
          return;
        } else if ((System.currentTimeMillis() - data.getLeftSpawn()) < 2_000L) {
          event.setCancelled(true);
          data.getCorePlayer().sendMessage("ffa.combat.spawnprot");
          return;
        } else {
          if (data.getSpawnTeleport() != 0
              && data.getSpawnTeleport() >= System.currentTimeMillis()) {
            data.getCorePlayer().sendMessage("ffa.teleport.cancelled.attacker");
            data.setSpawnTeleport(0);
            damager.playSound(damager.getLocation(), Sound.BLOCK_NOTE_BLOCK_DIDGERIDOO,
                1, 1);
            damager.spigot()
                .sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(""));
          }
        }
      }
    }

    if (event.getEntity() instanceof Player) {
      Player player = (Player) event.getEntity();
      if (damager != null && player.getUniqueId().equals(damager.getUniqueId())) {
        event.setCancelled(true);
        return;
      }

      FFAPlayer data = this.plugin.getPlayers().get(player.getUniqueId());
      if (data != null) {
        if (data.getState() == FFAPlayer.State.SPAWN
            || (System.currentTimeMillis() - data.getLeftSpawn()) < 2_000L) {
          event.setCancelled(true);
          if (damager != null) {
            FFAPlayer d1 = this.plugin.getPlayers().get(damager.getUniqueId());
            d1.getCorePlayer().sendMessage("ffa.combat.protection");
          }
        } else {
          if (data.getSpawnTeleport() != 0
              && data.getSpawnTeleport() >= System.currentTimeMillis()) {
            data.getCorePlayer().sendMessage("ffa.teleport.cancelled.attacked");
            data.setSpawnTeleport(0);
            player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_DIDGERIDOO, 1,
                1);
            player.spigot()
                .sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(""));
          }
        }
      }
    }
  }
}
