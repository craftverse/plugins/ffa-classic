package net.venade.games.ffa_classic.listener;

import java.security.SecureRandom;
import lombok.AllArgsConstructor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.inventories.SelectSpawnLocationMenu;
import net.venade.games.ffa_classic.kits.Kit;
import net.venade.games.ffa_classic.map.SpawnLocation;
import net.venade.games.ffa_classic.player.FFAPlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

/**
 * @author JakeMT04
 * @since 19/07/2021
 */
@AllArgsConstructor
public class InteractListener implements Listener {

    private final FFAClassicPlugin plugin;

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {
        if (event.toWeatherState()) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        if (event.getTo() == null) {
            return;
        }
        if (event.getTo().getBlockX() == event.getFrom().getBlockX()
            && event.getTo().getBlockZ() == event.getFrom().getBlockZ()) {
            return;
        }
        FFAPlayer player = this.plugin.getPlayers().get(event.getPlayer().getUniqueId());
        if (player == null) {
            event.getPlayer().kickPlayer("You aren't registered!");
            return;
        }
        if (player.getSpawnTeleport() != 0
            && player.getSpawnTeleport() >= System.currentTimeMillis()) {
            player.getCorePlayer().sendMessage("ffa.teleport.cancelled.moved");
            player.getBase()
                .playSound(player.getBase().getLocation(), Sound.BLOCK_NOTE_BLOCK_DIDGERIDOO, 1, 1);
            player.setSpawnTeleport(0);
            player.getBase().spigot()
                .sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(""));
        }
    }

    @EventHandler
    public void onThunderChange(ThunderChangeEvent event) {
        if (event.toThunderState()) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        if (event.getFoodLevel() <= 20) {
            event.setFoodLevel(20);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractAtEntityEvent event) {
        Player player = event.getPlayer();
        Entity entity = event.getRightClicked();
        if (entity.hasMetadata("ffa-kitselect")) {
            FFAPlayer data = this.plugin.getPlayers().get(player.getUniqueId());
            if (data == null) {
                return;
            }
            if (data.getState() != FFAPlayer.State.SPAWN) {
                return;
            }
            String kitName = entity.getMetadata("ffa-kitselect").get(0).asString();
            Kit kit = this.plugin.getKitManager().getKit(kitName);
            if (kit == null) {
                return;
            }
            event.setCancelled(true);
            player.playSound(player.getLocation(), Sound.ENTITY_ENDER_EYE_DEATH, 1, 1);
            new SelectSpawnLocationMenu(this.plugin, data, kit).openInventory(player);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onDamage(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof Player)) {
            return;
        }
        Player player = (Player) event.getDamager();
        Entity entity = event.getEntity();
        if (entity.hasMetadata("ffa-kitselect")) {
            FFAPlayer data = this.plugin.getPlayers().get(player.getUniqueId());
            if (data == null) {
                return;
            }
            if (data.getState() != FFAPlayer.State.SPAWN) {
                return;
            }
            String kitName = entity.getMetadata("ffa-kitselect").get(0).asString();
            Kit kit = this.plugin.getKitManager().getKit(kitName);
            if (kit == null) {
                return;
            }
            event.setCancelled(true);
            SecureRandom random = new SecureRandom();
            int position = random.nextInt(this.plugin.getMapManager().getSpawnLocations().size());
            SpawnLocation location = this.plugin.getMapManager().getSpawnLocations().get(position);
            data.teleportInWith(kit, location);
        }
    }
}
