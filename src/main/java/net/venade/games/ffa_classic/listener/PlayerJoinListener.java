package net.venade.games.ffa_classic.listener;

import lombok.AllArgsConstructor;
import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.player.FFAPlayer;
import net.venade.internal.api.scoreboard.test.VenadeBoard;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author JakeMT04
 * @since 15/07/2021
 */
@AllArgsConstructor
public class PlayerJoinListener implements Listener {

    private final FFAClassicPlugin plugin;

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        FFAPlayer player = new FFAPlayer(plugin, event.getPlayer(),
            new VenadeBoard(event.getPlayer()));
        this.plugin.getPlayers().put(event.getPlayer().getUniqueId(), player);
        this.plugin.getSpawnHandler().teleportToSpawn(player);
        event.setJoinMessage(null);
        this.plugin.getScoreboardHandler().registerInto(player);
        player.spawnHolograms();

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        FFAPlayer player = this.plugin.getPlayers().get(event.getPlayer().getUniqueId());
        if (player == null) {
            return;
        }
        if (player.getCombatTag() != 0 && player.getCombatTag() >= System.currentTimeMillis()) {
            event.getPlayer().setHealth(0);
        }
        event.setQuitMessage(null);
        plugin.getServer().getScheduler().runTaskLater(plugin, () -> {
            this.plugin.getPlayers().remove(event.getPlayer().getUniqueId());
        }, 5L);
    }
}
