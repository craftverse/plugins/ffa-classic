package net.venade.games.ffa_classic.listener;

import cloud.cubid.bridge.CubidCloud;
import cloud.cubid.module.service.group.CloudServiceGroupType;
import cloud.cubid.module.service.group.ICloudServiceGroup;
import cloud.cubid.module.service.service.ICloudService;
import lombok.AllArgsConstructor;
import net.minecraft.network.protocol.game.ClientboundCooldownPacket;
import net.venade.games.ffa_classic.FFAClassicPlugin;
import net.venade.games.ffa_classic.inventories.SelectKitMenu;
import net.venade.games.ffa_classic.player.FFAPlayer;
import net.venade.games.ffa_classic.player.FFAPlayer.State;
import net.venade.internal.api.VenadeAPI;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_19_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;

/**
 * @author JakeMT04
 * @since 15/07/2021
 */
@AllArgsConstructor
public class HotbarListener implements Listener {

  private final FFAClassicPlugin plugin;

  @EventHandler
  public void onDrop(PlayerDropItemEvent event) {
    event.setCancelled(true);
  }

  @EventHandler
  public void onSwap(PlayerSwapHandItemsEvent event) {
    FFAPlayer player = this.plugin.getPlayers().get(event.getPlayer().getUniqueId());
    if (player == null) {
      return;
    }
    if (player.getState() == State.SPAWN) {
      event.setCancelled(true);
    }
  }

  @EventHandler
  public void onClick(InventoryClickEvent event) {
    FFAPlayer player = this.plugin.getPlayers().get(event.getWhoClicked().getUniqueId());
    if (player == null) {
      return;
    }
    if (player.getState() == State.SPAWN) {
      if (event.getAction() == InventoryAction.HOTBAR_SWAP) {
        event.setCancelled(true);
        return;
      }
      if (event.getCurrentItem() != null) {
        if (event.getCurrentItem()
            .equals(this.plugin.getHotbarHandler().getBackToHub(player))
            || event.getCurrentItem()
            .equals(this.plugin.getHotbarHandler().getKitSelector(player))
            || event.getCurrentItem()
            .equals(this.plugin.getHotbarHandler().getAchievements(player))
            || event.getCurrentItem()
            .equals(this.plugin.getHotbarHandler().getMapRating(player))
            || event.getCurrentItem()
            .equals(this.plugin.getHotbarHandler().getTutorial(player))) {
          event.setCancelled(true);
        }
      }
    }
  }

  @EventHandler
  public void onPlace(BlockPlaceEvent event) {
    if (event.getPlayer().getGameMode() != GameMode.CREATIVE) {
      event.setCancelled(true);
    }
  }

  @EventHandler
  public void onBreak(BlockBreakEvent event) {
    if (event.getPlayer().getGameMode() != GameMode.CREATIVE) {
      event.setCancelled(true);
    }
  }

  public void applyCooldownTo(Player player, ItemStack itemStack) {
    net.minecraft.world.item.ItemStack stack = CraftItemStack.asNMSCopy(itemStack);
    ClientboundCooldownPacket packet = new ClientboundCooldownPacket(stack.getItem(), 3 * 20);
    ((CraftPlayer) player).getHandle().connection.send(packet);
  }

  @EventHandler
  public void onClick(PlayerInteractEvent event) {
    if (VenadeAPI.getInventoryManager().getInventory(event.getPlayer()).isPresent()) {
      return;
    }
    if (event.getAction() == Action.PHYSICAL) {
      return;
    }
    FFAPlayer player = this.plugin.getPlayers().get(event.getPlayer().getUniqueId());
    if (player == null) {
      event.getPlayer().kickPlayer("You aren't loaded!");
      return;
    }
    if (event.getPlayer().getInventory().getItemInMainHand()
        .equals(this.plugin.getHotbarHandler().getTutorial(player))) {
      event.setUseItemInHand(Result.DENY);
      event.setCancelled(true);
    } else if (event.getPlayer().getInventory().getItemInMainHand()
        .equals(this.plugin.getHotbarHandler().getKitSelector(player))) {
      event.setCancelled(true);
      new SelectKitMenu(plugin, player).openInventory(event.getPlayer());
    } else if (event.getPlayer().getInventory().getItemInMainHand()
        .equals(this.plugin.getHotbarHandler().getBackToHub(player))) {
      if (player.getHubTeleport() == 0
          || (System.currentTimeMillis() - player.getHubTeleport()) > 3_000L) {
        player.getCorePlayer().sendMessage("ffa.hub.again");
        event.getPlayer().playSound(event.getPlayer().getLocation(),
            Sound.BLOCK_BREWING_STAND_BREW, 1, 1);
        player.setHubTeleport(System.currentTimeMillis());
        applyCooldownTo(event.getPlayer(),
            event.getPlayer().getInventory().getItemInMainHand());
      } else {
        CubidCloud.getBridge()
            .getPlayerManager()
            .getOnlinePlayer(player.getBase().getUniqueId())
            .ifPresent(
                cubidPlayer -> {
                  final String serviceName = cubidPlayer.getConnectedService();
                  if (serviceName == null) {
                    return;
                  }

                  final String groupName = serviceName.split("-")[0];
                  final ICloudServiceGroup group =
                      CubidCloud.getBridge().getServiceManager().getCloudGroup(groupName)
                          .orElse(null);

                  if (group != null
                      && (group.getServerGroupType() == CloudServiceGroupType.LOBBY
                      || group.getServerGroupType()
                      == CloudServiceGroupType.STATIC_LOBBY)) {
                    cubidPlayer.sendLanguageMessage("cubid.message.hub.already");
                    return;
                  }

                  final ICloudService hub =
                      CubidCloud.getBridge().getServiceManager()
                          .findFallbackService(cubidPlayer.getRegion())
                          .orElse(null);
                  if (hub == null) {
                    cubidPlayer.sendLanguageMessage("cubid.message.hub.error");
                    return;
                  }

                  cubidPlayer.connect(hub);
                });
      }
    }
  }
}
